FROM postgres:14
RUN apt update -y
RUN apt install curl nano ca-certificates -y --no-install-recommends
WORKDIR /tmp
RUN curl https://stepik.org/media/attachments/lesson/702368/dump.sql --output dump.sql
ENV PGPASSWORD='postgres'

WORKDIR /home/ubuntu/postgres/data/
COPY setup.sh /tmp
#RUN psql  -U postgres deadline < /tmp/dump.sql
#RUN psql "postgres://postgres:postgres@localhost:5432/deadline"

# shared_preload_libraries = 'pg_stat_statements'
# pg_stat_statements.track = all
# pg_stat_statements.max = 10000
# track_activity_query_size = 2048

# RUN echo "shared_preload_libraries = 'pg_stat_statements'" >> ./postgresql.conf
# RUN echo "pg_stat_statements.track = all" >> ./postgresql.conf
# RUN echo "pg_stat_statements.max = 10000" >> ./postgresql.conf
# RUN echo "track_activity_query_size = 2048" >> ./postgresql.conf