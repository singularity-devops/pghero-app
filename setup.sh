psql -U postgres deadline < /tmp/dump.sql

echo "shared_preload_libraries = 'pg_stat_statements'" >> /home/ubuntu/postgres/data/postgresql.conf
echo "pg_stat_statements.track = all" >> /home/ubuntu/postgres/data/postgresql.conf
echo "pg_stat_statements.max = 10000" >> /home/ubuntu/postgres/data/postgresql.conf
echo "track_activity_query_size = 2048" >> /home/ubuntu/postgres/data/postgresql.conf