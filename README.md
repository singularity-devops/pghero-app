# pghero-demo

## About

**PgHero** - это замечательный дэшборд для получения информации по поведению PostgreSQL. В нем можно узнать проблемные места в базе данных и получить рекомендации по оптимизации состояния БД.

## Tasks

- запускает контейнеры для postgres и для pghero;
- postgres должен запускать образ **postgres:14** и иметь имя *jusan-postgres*;
- pghero должен запускать образ **ankane/pghero:v2.8.2** и иметь имя *jusan-pghero*;
- postgres должен поднимать бэкап в базу данных deadline;
- postgres имеет порты 5432:5432;
- pghero имеет порты 8080:8080;
- можно оформить в docker-compose.yml

## Execution

First, enter into the container named **jusan-postgres**:
```bash
docker exec -it jusan-postgres bash
```
and run the following commands:

```bash
cp /tmp/setup.sh .
./setup.sh
```

## Screenshots

![pghero](https://user-images.githubusercontent.com/54265853/182116315-65d592ea-6002-4991-907a-dc93e0ad79c5.png)
